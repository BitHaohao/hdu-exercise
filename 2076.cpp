#include <cstdio>
#include <cmath>

int main() {
  int n = 0;
  double h, m, s, angh, angm, diff;
  while (EOF != scanf("%d", &n)) {
    while (n--) {
      scanf("%lf%lf%lf", &h, &m, &s);
      h = h > 12.0 ? h - 12.0 : h;
      angh = h + m / 60.0 + s / 3600.0;
      angm = m + s / 60.0;
      diff = fabs(angh * 30 - angm * 6);
      diff = diff > 180 ? 360 - diff : diff;
      printf("%d\n", int(diff));
    }
  }
  return 0;
}
