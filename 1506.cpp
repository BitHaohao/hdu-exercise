/* Largest Rectangle in a Histogram
 * Rect(cur) = H[cur] * (j - i + 1), i <= cur <= j,
 *                                   H[i:cur] <= H[cur] 
 *                                   H[cur:j] >= H[cur]
 * 可以分解成两个问题, 左侧大于等于当前点的最小索引
 *                     右侧大于等于当前点的最大索引
 * 然后根据公式, 可以求解出当前点所对应的最大矩阵面积, 找出所有点中最大值即可.
 *
 * Tag: DP 双向 一维
 */
#include <cstdio>

#define MAX_N 100002

int main() {
  long long H[MAX_N] = {0};
  int L[MAX_N] = {0};
  int R[MAX_N] = {0};
  int n = 0;
  while (EOF != scanf("%d", &n) && n) {
    for (int i = 1; i <= n; ++ i) {
      L[i] = R[i] = i;
      scanf("%lld", H + i);
    }

    H[0] = H[n + 1] = -1;
    for (int i = 1; i <= n; ++ i) {
      while (H[L[i] - 1] >= H[i]) {
        L[i] = L[L[i]  - 1];
      }
    }
    for (int i = n; i >= 1; -- i) {
      while (H[R[i] + 1] >= H[i]) {
        R[i] = R[R[i] + 1];
      }
    }
    long long max = 0;
    for (int i = 1; i <= n; ++ i) {
      long long cur = H[i] * (R[i] - L[i] + 1);
      if (max < cur) {
        max = cur;
      }
    }
    printf("%lld\n", max);
  }
  return 0;
}

