/* 不容易系列之(3)—— LELE的RPG难题
 * f(n) = f(n - 1) + 2 * f(n -2)
 */
#include <cstdio>

#define MAX_N 50

typedef long long int64;

int main() {
  static int64 steps[MAX_N] = {3, 6, 6};
  for (int i = 3; i < MAX_N; ++ i) {
    steps[i] = steps[i - 1] + 2 * steps[i - 2];
  }
  int n = 0;
  while(EOF != scanf("%d", &n) && n) {
    printf("%lld\n", steps[n - 1]);
  }
  return 0;
}
