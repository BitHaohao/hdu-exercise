/* 1000 A+B Problem
 */
#include <cstdio>

int main() {
  int a = 0, b = 0;
  while(EOF != scanf("%d %d", &a, &b)) {
    printf("%d\n", a + b);
  }
  return 0;
}

