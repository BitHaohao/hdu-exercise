/* Hano V
 * f(n) = pow(2, n - k)
 */
#include <cstdio>

#define MAX_N 61
typedef unsigned long long uint64;

void func_hanov(uint64 *steps) {
  steps[0] = 1;
  uint64 tmp = 1;
  for (int i = 1; i < MAX_N; ++ i) {
    tmp *= 2;
    steps[i] = tmp;
  }
}

int main() {
  uint64 pre_steps[MAX_N];
  func_hanov(pre_steps);
  int n = 0, x = 0, y = 0;
  while(EOF != scanf("%d", &n)) {
    while (n --) {
      scanf("%d%d", &x, &y);
      printf("%llu\n", pre_steps[x - y]);
    }
  }
  return 0;
}
