/* Subset sequence
 * Sub-array number F[n] from a n number array:
 *    F[n] = n * (F[n - 1] + 1); F[0] = 0;
 * Takes a n = 3, m = 10 as example:
 *  -> {1}
 *     {1, 2}
 *     {1, 2, 3}
 *     {1, 3}
 *     {1, 3, 2}
 *  -> {2}
 *     {2, 1}
 *     {2, 1, 3}
 *     {2, 3}
 *     {2, 3, 1}
 *  -> {3}
 *     {3, 1}
 *     {3, 1, 2}
 *     {3, 2}
 *     {3, 2, 1}
 *  total 15 sub-arrays, and they can be divide into 3 Groups, in each group,
 *    first element are the same, and lasts elements can be seem as a (n-1) array layout.
 */
#include <cstdio>

#define MAX_LEN 21

int main() {
  int n = 0, arr[MAX_LEN] = { 0 };
  __int64 m = 0;
  __int64 fn[MAX_LEN] = { 0 };
  for (int i = 1; i < MAX_LEN; ++i) {
    fn[i] = (i - 1) * fn[i - 1] + 1;
  }
  while (EOF != scanf("%d%I64d", &n, &m)) {
    for (int i = 0; i < MAX_LEN; ++i) {
      arr[i] = i;
    }
    while (n && m) {
      int tmp = m / fn[n] + ((m % fn[n]) ? 1 : 0);
      if (tmp) {
        printf("%d", arr[tmp]);
        //
        int i = tmp;
        for (; i < n; ++i) {
          arr[i] = arr[i + 1];
        }
        m -= (tmp - 1) * fn[i] + 1;
        printf(m ? " " : "\n");
      }
      --n;
    }
  }
  return 0;
}