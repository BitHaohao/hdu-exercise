/* 不要62
 * 参考链接: [初探数位dp](http://wenku.baidu.com/view/9de41d51168884868662d623)
 */
#include <cstdio>
#include <cstring>

#define MAX_LEN 7

void func_init(int fseq[MAX_LEN + 1][10]) {
  memset(fseq, 0, sizeof(fseq));
  fseq[0][0] = 1;
  for (int i = 1; i <= MAX_LEN; ++ i) {
    for (int j = 0; j <= 9; ++ j) {
      for (int k = 0; k <= 9; ++ k) {
        if (j != 4 && !(j == 6 && k == 2)) {
          fseq[i][j] += fseq[i - 1][k];
        }
      }
    }
  }
}

int func_num(int num, const int fseq[8][10]) {
  static int digit[9] = {0};
  int len = 0;
  while (num > 0) {
    digit[++ len] = num % 10;
    num /= 10;
  }
  digit[len + 1] = 0;

  int ans = 0;
  for (int i = len; i > 0; -- i) {
    for (int j = 0; j <= digit[i] - 1; ++ j) {
      if (j != 4 && !(j == 2 && digit[i + 1] == 6)) {
        ans += fseq[i][j];
      }
    }
    if (digit[i] == 4 || (digit[i] == 2 && digit[i + 1] == 6)) {
      break;
    }
  }
  return ans;
}

int main() {
  int frm = 0, to = 0;
  static int fseq[MAX_LEN + 1][10];
  func_init(fseq);
  while (EOF != scanf("%d%d", &frm, &to) && (frm | to)) {
    printf("%d\n", func_num(to + 1, fseq) - func_num(frm, fseq));
  }
  return 0;
}
