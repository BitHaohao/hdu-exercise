/* A+B for Input-Output Practice (IV)
 */
#include <cstdio>

int main() {
  int n = 0, a = 0, sum = 0;
  while (EOF != scanf("%d", &n) && n) {
    while(n --) {
      scanf("%d", &a);
      sum += a;
    }
    printf("%d\n", sum);
    sum = 0;
  }
  return 0;
}

