#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <climits>
#include <algorithm>

#define MAX_N 65
typedef unsigned long long uint64;

void func_hanoii(uint64 *steps) {
  steps[0] = 0;
  steps[1] = 1;
  steps[2] = 3;
  for (int i = 3; i < MAX_N; ++ i) {
    uint64 min_steps = ULLONG_MAX;
    for (int j = 1; j < i; ++ j) {
      min_steps = std::min(min_steps,
                           (steps[j] << 1) + ((uint64)1 << (i - j)));
    }
    steps[i] = min_steps - 1;
  }
}

int main() {
  uint64 pre_steps[MAX_N];
  func_hanoii(pre_steps);
  int n = 0;
  while(EOF != scanf("%d", &n)) {
    printf("%llu\n", pre_steps[n]);
  }
  return 0;
}

#if 0
#include <cstdio>
#include <cstdlib>

#define MAX_N 65
typedef unsigned long long uint64;

int main() {
  uint64 pre_steps[MAX_N] = {
    0, 1, 3, 5, 9,
    13, 17, 25, 33, 41,
    49, 65, 81, 97, 113,
    129, 161, 193, 225, 257,
    289, 321, 385, 449, 513,
    577, 641, 705, 769, 897,
    1025, 1153, 1281, 1409, 1537,
    1665, 1793, 2049, 2305, 2561,
    2817, 3073, 3329, 3585, 3841,
    4097, 4609, 5121, 5633, 6145,
    6657, 7169, 7681, 8193, 8705,
    9217, 10241, 11265, 12289, 13313,
    14337, 15361, 16385, 17409, 18433
  };
  int n = 0;
  while(EOF != scanf("%d", &n)) {
    printf("%llu\n", pre_steps[n]);
  }
  return 0;
}
#endif

