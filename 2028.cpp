/* Lowest Common Multiple Plus
 * 注意数据范围
 */
#include <cstdio>
#include <utility>
#include <ctype.h>

#define MAX_LEN 100

typedef unsigned long uint64;

static inline
uint64 func_gcd(uint64 a, uint64 b) {
  if (a < b) {
    std::swap(a, b);
  }
  if (b == 0) {
    return a;
  } else {
    return func_gcd(b, a % b);
  }
}

static inline
uint64 func_lcm(uint64 a, uint64 b) {
  return a * b / func_gcd(a, b);
}

int main() {
  int n = 0;
  uint64 cur = 0, tar = 1;
  while(EOF != scanf("%d\n", &n) && n) {
    while (n --) {
      scanf("%lu", &cur);
      tar = func_lcm(cur, tar);
    }
    printf("%lu\n", tar);
    tar = 1;
  }
}
