#include <cstdio>
#include <cstring>
#include <algorithm>

#define MAX_N 81

int main() {
  char arr[MAX_N];
  char letter[2][2];
  int n = 0;
  bool first = true;
  while (EOF != scanf("%d%s%s", &n, letter[0], letter[1])) {
    if (!first) {
      putchar('\n');
    }
    first = false;
    if (1 == n) {
      puts(letter[0]);
      continue;
    }
    int taridx = ((n - 1) >> 1) & 0x1;
    if (taridx) {
      std::swap(letter[0][0], letter[1][0]);
    }
    memset(arr, letter[0][0], n);
    arr[n] = 0;
    arr[0] = arr[n - 1] = 32;

    puts(arr);
    arr[0] = arr[n - 1] = letter[0][0];
    int i = 0;
    for (i = 1; i <= (n / 2); ++i) {
      memset(arr + i, letter[i & 0x1][0], n - 2 * i);
      puts(arr);
    }
    for (i = i - 2; i >= 1; --i) {
      memset(arr + i, letter[i & 0x1][0], n - 2 * i);
      puts(arr);
    }
    memset(arr, letter[0][0], n);
    arr[0] = arr[n - 1] = 32;
    puts(arr);
  }
  return 0;
}

