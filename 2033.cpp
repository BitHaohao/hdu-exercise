/* 人见人爱A+B
 */
#include <cstdio>

static inline
void timeSum(int &ah, int &am, int &as,
             int bh, int bm, int bs) {
  int h = 0, m = 0;
  as += bs;
  m = as / 60;
  as %= 60;
  am += bm + m;
  h = am / 60;
  am %= 60;
  ah += bh + h;
}

int main() {
  int n = 0;
  int ah = 0, am = 0, as = 0;
  int bh = 0, bm = 0, bs = 0;
  while (EOF != scanf("%d", &n)) {
    while(n --) {
      scanf("%d%d%d%d%d%d", &ah, &am, &as, &bh, &bm, &bs);
      timeSum(ah, am, as, bh, bm, bs);
      printf("%d %d %d\n", ah, am, as);
    }
  }
  return 0;
}

