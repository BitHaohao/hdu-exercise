/* ��� III
 * f(n) = f(n - 1) * 3 + 2
 * although this will results in f(n) = pow(3, n) - 1
 * but with c++ standard library, results will not match when n comes bigger.
 */
#include <cstdio>
#include <cstdlib>
#include <cmath>

#define MAX_N 36
typedef unsigned long long uint64;

void func_hanoiii(uint64 *steps) {
  steps[0] = 0;
  steps[1] = 2;
  for (int i = 2; i < MAX_N; ++ i) {
    steps[i] = steps[i - 1] * 3 + 2;
  }
}

int main() {
  uint64 pre_steps[MAX_N];
  func_hanoiii(pre_steps);
  int n = 0;
  while(EOF != scanf("%d", &n)) {
    printf("%llu\n", pre_steps[n]);
  }
  return 0;
}
