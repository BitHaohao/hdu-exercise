/* 超级楼梯
 */
#include <cstdio>

#define MAX_STAIRS 40

int main() {
  static int ways[MAX_STAIRS + 2] = {0, 1, 1};
  for (int i = 3; i <= MAX_STAIRS; ++ i) {
    ways[i] = ways[i - 1] + ways[i - 2];
  }
  int n = 0, m = 0;
  while(EOF != scanf("%d", &n)) {
    while(n --) {
      scanf("%d", &m);
      printf("%d\n", ways[m]);
    }
  }
  return 0;
}

