/* 人见人爱A^B
 * Exp-by-squaring: https://en.wikipedia.org/wiki/Exponentiation_by_squaring
 */
#include <cstdio>

static inline
int func(int a, int n) {
  int x = a % 1000;
  if (n == 1) {
    return x;
  }
  int y = 1;
  while (n > 1) {
    if (n & 0x1) {
      y = (x * y) % 1000;
      x = (x * x) % 1000;
      n = (n - 1) >> 1;
    } else {
      x = (x * x) % 1000;
      n = n >> 1;
    }
  }
  return (x * y) % 1000;
}

int main() {
  int x = 0, n = 0;
  while (EOF != scanf("%d%d", &x, &n) && (x | n)) {
    printf("%d\n", func(x, n));
  }
  return 0;
}

