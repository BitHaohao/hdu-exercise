/* 三角形
 * Variable a/b/c shall use double type instead of int type.
 */
#include <cstdio>

int main() {
  int n = 0;
  double a = 0, b = 0, c = 0;
  while (EOF != scanf("%d\n", &n)) {
    while (n --) {
      scanf("%lf%lf%lf", &a, &b, &c);
      printf("%s\n", a + b > c && a + c > b && b + c > a ? "YES" : "NO");
    }
  }
  return 0;
}


