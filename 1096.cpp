/* A+B for Input-Output Practice (V)
 */
#include <cstdio>

int main() {
  int n = 0, m = 0, a = 0, sum = 0;
  while (EOF != scanf("%d", &n) && n) {
    while(n --) {
      scanf("%d", &m);
      while (m --) {
        scanf("%d", &a);
        sum += a;
      }
      if (n != 0) {
        printf("%d\n\n", sum);
      } else {
        printf("%d\n", sum);
      }
      sum = 0;
    }
  }
  return 0;
}

