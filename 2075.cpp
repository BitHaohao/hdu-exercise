#include <cstdio>

int main() {
  int n = 0, x = 0, y = 0;
  while (EOF != scanf("%d", &n) && n) {
    while (n--) {
      scanf("%d%d", &x, &y);
      printf("%s\n", x % y ? "NO" : "YES");
    }
  }
  return 0;
}
