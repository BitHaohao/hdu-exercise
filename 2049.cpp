/* 不容易系列之(4)——考新郎
 * 同 2048， 只是加上要从N个人中选出M个人， 然后在把M个人按2048计算
 * f(1) = 0, f(2) = 1; f(n) = (n - 1) * (f(n-1) + f(n-2)), n >= 3;
 */
#include <cstdio>

#define MAX_N 21
typedef unsigned long long uint64;

int main() {
  static uint64 badsitu[MAX_N + 1] = {0, 0, 1};
  for (int i = 3; i < MAX_N; ++ i) {
    badsitu[i] = (i - 1) * (badsitu[i - 1] + badsitu[i - 2]);
  }
  int C = 0, M = 0, N = 0;
  uint64 total = 1;
  while (EOF != scanf("%d", &C) && C) {
    while(C --) {
      scanf("%d%d", &N, &M);
      if (M != N) {
        for (int i = N, j = M; j > 0; j --, i --) {
          total *= i;
        }
        for (int i = M; i > 0; i --) {
          total /= i;
        }
      }
      printf("%lld\n", total * badsitu[M]);
      total = 1;
    }
  }
  return 0;
}
