/* 母牛的故事
 * Formula: F(1) = 1; F(2) = 2; F(3) = 3; F(n) = F(n-1) + F(n-3), n > 3;
 */
#include <cstdio>

#define MAX_YEARS 55

int main() {
  static int cow[1 + MAX_YEARS] = {0, 1, 2, 3};
  int n = 0;
  for (int i = 4; i < MAX_YEARS; ++ i) {
    cow[i] = cow[i - 1] + cow[i - 3];
  }
  while (EOF != scanf("%d", &n) && n) {
    printf("%d\n", cow[n]);
  }
  return 0;
}

