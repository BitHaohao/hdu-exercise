#include <cstdio>
#include <cmath>
#include <algorithm>

int main() {
  int n = 0;
  const double sqrt2 = sqrt(2);
  int x1, y1, x2, y2;
  while (EOF != scanf("%d", &n)) {
    while (n --) {
      scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
      int n1 = x1 + y1;
      int n2 = x2 + y2;
      if (n1 > n2) {
        std::swap(x1, x2);
        std::swap(y1, y2);
        std::swap(n1, n2);
      }

      if (n1 == n2) {
        printf("%.3lf\n", abs(x1 - x2) * sqrt2);
      } else {
        double sum1 = (x2 - x1) * sqrt2;
        double sum2 = 0.0;
        int nx = 0;
        for (int i = n1; i < n2; ++ i) {
          nx += i;
          sum2 += sqrt(2 * i * (i + 1) + 1.0);
        }
        sum2 += nx * sqrt2;
        printf("%.3f\n", sum2 + sum1);
      }
    }
  }
  return 0;
}

