/* 计算球体积
 * Note that if use float type instead of a double type, it will be a wrong answer
 *    due to precision problems.
 */
#include <cstdio>
#include <cmath>

#define PI 3.1415927

int main() {
  const double common = 4 * PI / 3;
  double r = 0;
  while (EOF != scanf("%lf", &r)) {
    printf("%.3lf\n", common * r * r * r);
  }
  return 0;
}

