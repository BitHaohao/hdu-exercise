/* 龟兔赛跑
 * 将起点和终点也看作两个充电点， 所以共有 N + 2 个充电点
 * 状态转移方程：  DP[i] = min{Dp[j] + t[j, i]}, DP[0] = 0;
 */
#include <cstdio>
#include <cmath>

#define MAX_CHARGE 100

int main() {
  int L = 0;                          // total length
  int N = 0,                          // charge station number
      C = 0,                          // length after each charging
      T = 0;                          // time costs on each charging
  int VR = 0,                         // rabbit speed
      VT1 = 0,                        // tortoise fast speed
      VT2 = 0;                        // tortoise slow speed
  int STA[MAX_CHARGE + 2] = { 0 };    // charge station position
  double DP[MAX_CHARGE + 2] = { 0.f };
  bool failed = false;

  while (EOF != scanf("%d", &L)) {
    scanf("%d%d%d", &N, &C, &T);
    scanf("%d%d%d", &VR, &VT1, &VT2);
    for (int i = 1; i <= N; ++i) {
      scanf("%d", STA + i);
    }
    STA[N + 1] = L;
    DP[0] = 0.f;
    failed = false;
    double r_time = (double)L / VR;
    double c_time = (double)C / VT1;

    for (int i = 1; i < N + 2; ++i) {
      DP[i] = r_time + 10.f; // set max to rabbit time
      for (int j = 0; j < i; ++j) {
        double len = STA[i] - STA[j];
        double t_time = 0;
        if (len <= C) {
          t_time = len / VT1;
        }
        else {
          t_time = c_time + (len - C) / VT2;
        }
        t_time += DP[j];
        if (0 != j) { // except first charge station, each charging costs time T
          t_time += T;
        }
        if (t_time < DP[i]) {
          DP[i] = t_time;
        }
      }
      if (DP[i] > r_time) { // if time costs after reach one station already exceeds rabbit's, rabbit win
        failed = true;
        break;
      }
    }
    printf("%s\n", failed ? "Good job,rabbit!" : "What a pity rabbit!");
  }
  return 0;
}
