/* Hano VI
 * f(n) = pow(3, n)
 */
#include <cstdio>

#define MAX_N 31
typedef unsigned long long uint64;

void func_hanovi(uint64 *steps) {
  steps[0] = 0;
  uint64 tmp = 1;
  for (int i = 1; i < MAX_N; ++ i) {
    tmp *= 3;
    steps[i] = tmp;
  }
}

int main() {
  uint64 pre_steps[MAX_N];
  func_hanovi(pre_steps);
  int n = 0, x = 0;
  while(EOF != scanf("%d", &n)) {
    while (n --) {
      scanf("%d", &x);
      printf("%llu\n", pre_steps[x]);
    }
  }
  return 0;
}
