/* 青年歌手大奖赛_评委会打分
 * Note the initialize of max & min val, it shall get from input, god knows what
 *   the max & min value of the test case are.
 */
#include <cstdio>

int main() {
  int n = 0, x = 0, high = 0, low = 0;
  int m = 0, sum = 0;
  while (EOF != scanf("%d", &n)) {
    m = n;
    -- n;
    scanf("%d", &x);
    high = low = x;
    sum += x;
    while (n --) {
      scanf("%d", &x);
      if (high < x) { high = x; }
      if (low > x) { low = x; }
      sum += x;
    }
    printf("%.2lf\n", (double)(sum - high - low) / (m - 2));
    sum = 0;
  }
  return 0;
}
