/* A+B for Input-Output Practice (VI)
 */
#include <cstdio>

int main() {
  int m = 0, a = 0, sum = 0;
  while (EOF != scanf("%d", &m) && m) {
    while (m --) {
      scanf("%d", &a);
      sum += a;
    }
    printf("%d\n", sum);
    sum = 0;
  }
  return 0;
}

