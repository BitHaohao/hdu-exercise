/* 蟠桃记
 */
#include <cstdio>

int main() {
  int n = 0, ori = 1;
  while (EOF != scanf("%d", &n)) {
    for (int i = 1; i < n; ++ i) {
      ori = (ori + 1) << 1;
    }
    printf("%d\n", ori);
    ori = 1;
  }
  return 0;
}

