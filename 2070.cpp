#include <cstdio>

typedef unsigned long long uint64;
#define MAX_N 51

void func_fibo(uint64 *arr) {
  arr[0] = 0;
  arr[1] = 1;
  for (int i = 2; i < MAX_N; ++ i) {
    arr[i] = arr[i - 1] + arr[i - 2];
  }
}

int main() {
  int n = 0;
  uint64 fibo[MAX_N];
  func_fibo(fibo);
  while(EOF != scanf("%d", &n) && n != -1) {
    printf("%llu\n", fibo[n]);
  }
  return 0;
}

