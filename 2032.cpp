#include <cstdio>
#include <cstring>

int main() {
  int n = 0;
  static  int tria[32] = {0};
  while (EOF != scanf("%d", &n)) {
    memset(tria, 0, sizeof(tria));
    tria[0] = 1;
    for (int  i = 0; i < n; ++ i) {
      putchar('1');
      for (int j = i; j; -- j) {
        tria[j] += tria[j - 1];
      }
      for (int j = 1; j <= i; ++j) {
        printf(" %d", tria[j]);
      }
      putchar('\n');
    }
    putchar('\n');
  }
  return 0;
}
