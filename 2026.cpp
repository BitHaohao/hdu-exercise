#include <cstdio>
#include <ctype.h>

#define MAX_LEN 100

int main() {
  static char entity[MAX_LEN + 2] = {' '};
  char max = 0;
  while(gets(entity + 1)) {
    for (int i = 1; entity[i]; ++ i) {
      if (isalpha(entity[i]) && entity[i - 1] == ' ') {
        putchar(toupper(entity[i]));
      } else {
        putchar(entity[i]);
      }
    }
    putchar('\n');
    max = 0;
  }
  return 0;
}
