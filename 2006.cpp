/* 求奇数的乘积 
 */
#include <cstdio>

int main() {
  int result = 1, n = 0, i = 0;
  while(EOF != scanf("%d", &n) && n) {
    while(n --) {
      scanf("%d", &i);
      if (i & 0x1) {
        result *= i;
      }
    }
    printf("%d\n", result);
    result = 1;
  }
  return 0;
}

