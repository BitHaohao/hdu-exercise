/* 神、上帝以及老天爷
 * 错排问题： https://zh.wikipedia.org/zh/%E9%94%99%E6%8E%92%E9%97%AE%E9%A2%98
 * f(1) = 0, f(2) = 1; f(n) = (n - 1) * (f(n-1) + f(n-2)), n >= 3;
 */
#include <cstdio>

#define MAX_N 21
typedef unsigned long long uint64;

int main() {
  static uint64 badsitu[MAX_N + 1] = {0, 0, 1};
  static double badper[MAX_N + 1] = {0.0, 0.0, 50};
  double total = 2 / 100.0;
  for (int i = 3; i < MAX_N; ++ i) {
    total *= i;
    badsitu[i] = (i - 1) * (badsitu[i - 1] + badsitu[i - 2]);
    badper[i] = badsitu[i] / total;
  }
  int C = 0, N = 0;
  while (EOF != scanf("%d", &C) && C) {
    while(C --) {
      scanf("%d", &N);
      printf("%.2lf%%\n", badper[N]);
    }
  }
  return 0;
}
