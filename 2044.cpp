/* 一只小蜜蜂...
 * 斐波那契数列，需要使用64位整数
 */
#include <cstdio>

#define MAX_N 50

typedef long long int64;

int main() {
  static int64 steps[MAX_N] = {1, 1};
  for (int i = 2; i < MAX_N; ++ i) {
    steps[i] = steps[i - 1] + steps[i - 2];
  }
  int frm = 0, to = 0, n = 0;
  while(EOF != scanf("%d", &n) && n) {
    while (n --) {
      scanf("%d%d", &frm, &to);
      to -= frm;
      printf("%lld\n", steps[to]);
    }
  }
  return 0;
}
