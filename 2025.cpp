#include <cstdio>
#include <cstring>

#define MAX_LEN 100

int main() {
  static char entity[MAX_LEN + 1] = {0};
  char max = 0;
  while(gets(entity)) {
    for (int i = 0; entity[i]; ++ i) {
      if (entity[i] > max) {
        max = entity[i];
      }
    }
    for (int i = 0; entity[i]; ++ i) {
      putchar(entity[i]);
      if (entity[i] == max) {
        printf("(max)");
      }
    }
    putchar('\n');
    max = 0;
  }
  return 0;
}
