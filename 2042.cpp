/* 不容易系列之二
 */
#include <cstdio>

#define MAX_N 30

int main() {
  static int sheep[MAX_N + 2] = {3};
  for (int i = 1; i <= MAX_N; ++ i) {
    sheep[i] = (sheep[i - 1] - 1) << 1;
  }
  int n = 0, m = 0;
  while(EOF != scanf("%d", &n)) {
    while(n --) {
      scanf("%d", &m);
      printf("%d\n", sheep[m]);
    }
  }
  return 0;
}

