/* B-number
 */
#include <cstdio>
#include <cstring>

#define MAX_LEN 11

// num = uvwxyz => digit = [0, z, y, x, w, v, u, 0]
int digit[MAX_LEN], len;
// dp 第3维度:
//            0 : 没有出现13;
//            1 : 没有出现13, 但首位为3;
//            2 : 出现13;
int dp[MAX_LEN + 2][13][3];

// pos : 表示当前为第pos位数
// pre : 前一个数字
// mod : 表示余数
// stat : 是否已经出现了13, 默认没有=false
// flag : 表示数据范围是否有限制, 默认有限制=true
int dfs(int pos, int pre, int mod, bool stat, bool flag) {
  if (pos <= 0) { // 若所有数都已经判断过了, 检查是否已经出现了'13' 和 余数为0
    return stat && (mod == 0);
  }
  if (!flag && stat && dp[pos][mod][0] != -1) {
    return dp[pos][mod][0];
  }
  if (!flag && !stat && pre != 1 && dp[pos][mod][2] != -1) {
    return dp[pos][mod][2];
  }
  if (!flag && !stat && pre == 1 && dp[pos][mod][1] != -1) {
    return dp[pos][mod][1];
  }
  int end = flag ? digit[pos] : 9;
  int ret = 0;
  for (int i = 0; i <= end; ++ i) {
    ret += dfs(pos - 1, i, (mod * 10 + i)%13, stat || (pre == 1 && i == 3), flag && (i == end));
  }
  if (!flag) {
    if (pre == 1 && !stat) {
      dp[pos][mod][1] = ret;
    }
    if (pre != 1 && !stat) {
      dp[pos][mod][2] = ret;
    }
    if (stat) {
      dp[pos][mod][0] = ret;
    }
  }
  return ret;
}

int getCount(int num) {
  len = 0;
  while (num > 0) {
    digit[++ len] = num % 10;
    num /= 10;
  }
  return dfs(len, 0, 0, false, true);
}

int main() {
  int n = 1314;
  memset(dp, -1, sizeof(dp));
  while(EOF != scanf("%d", &n)) {
    printf("%d\n", getCount(n));
  }
  return 0;
}
