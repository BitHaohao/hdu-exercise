/* A + B Again
 */
#include<cstdio>

typedef long long int64;

int main(void)
{
  int64 a = 0, b = 0;
  while (EOF != scanf("%I64x%I64X", &a, &b))
    printf(a + b < 0 ? "-%I64X\n" : "%I64X\n", a + b < 0 ? - (a + b) : (a + b));
  return 0;
}
