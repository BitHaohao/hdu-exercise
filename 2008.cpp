/* 数值统计
 */
#include <cstdio>

int main() {
  int pos = 0, neg = 0, zeo = 0;
  int n = 0;
  float x = 0;
  while(EOF != scanf("%d", &n) && n) {
    while(n --) {
      scanf("%f", &x);
      pos += x > 0;
      neg += x < 0;
      zeo += x == 0;
    }
    printf("%d %d %d\n", neg, zeo, pos);
    neg = zeo = pos = 0;
  }
  return 0;
}

