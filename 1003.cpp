/* Max Sum
 * Start index is different from each time's start index.
 */
#include <cstdio>

int main() {
  int pre = 0, cur = 0;
  int m = 0, n = 0, max = 0;
  int start = 1, end = 1, temp = 1;
  while (EOF != scanf("%d", &m) && m) {
    for (int j = 1; j <= m; ++ j) {
      scanf("%d", &n);
      scanf("%d", &pre);
      max = pre;
      for (int i = 2; i <= n; ++ i) {
        scanf("%d", &cur);
        if (cur > cur + pre) {
          temp = i;
        } else {
          cur += pre;
        }
        if (max < cur) {
          max = cur;
          start = temp;
          end = i;
        }
        pre = cur;
      }
      printf("Case %d:\n%d %d %d\n", j, max, start, end);
      if (j < m) {
        printf("\n");
      }
      start = end = temp = 1;
    }
  }
  return 0;
}

