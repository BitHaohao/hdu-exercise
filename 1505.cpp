/* City Game
 * 首先:
 * 0 1 1 1 1 1                 0 1 1 1 1 1
 * 1 1 1 1 1 1                 1 2 2 2 2 2
 * 0 0 0 1 1 1  <纵向累加> ==> 0 0 0 3 3 3
 * 1 1 1 1 1 1                 1 1 1 4 4 4
 * 1 1 1 1 1 1                 2 2 2 5 5 5
 * 此时题目转换成1506
 *
 * 其次, 将每行元素看成一个直方图, 找出最大区间范围
 *
 * 最后, 应用1506中公式确定当前点的最大区域面积, 确定最大值
 *
 * Tag: DP 分解
 */

#include <cstdio>

#define MAX_SIZE 1000

int main() {
  short H  [MAX_SIZE][MAX_SIZE] = {0};
  short X_L[MAX_SIZE][MAX_SIZE] = {0};
  short X_R[MAX_SIZE][MAX_SIZE] = {0};
  int k = 0;
  int m = 0, n = 0;
  char elem = 0;
  while (EOF != scanf("%d", &k) && k) {
    while (k --) {
      scanf("%d %d\n", &m, &n);
      int i = 0, j = 0;
      for (i = 0; i < m; ++ i) {
        for (j = 0; j < n - 1; ++ j) {
          scanf("%c ", &elem);
          H[i][j] = elem == 'F' ? 1 : 0;
          X_L[i][j] = X_R[i][j] = j;
        }
        scanf("%c\n", &elem);
        H[i][j] = elem == 'F' ? 1 : 0;
        X_L[i][j] = X_R[i][j] = j;
      }

      for (i = 1; i < m; ++ i) {
        for (j = 0; j < n; ++ j) {
          if (H[i][j] != 0) {
            if (H[i - 1][j] != 0) {
              H[i][j] += H[i-1][j];
            }
          }
        }
      }

      for (i = 0; i < m; ++ i) {
        for (j = 1; j < n; ++ j) {
          while (H[i][X_L[i][j] - 1] >= H[i][j]) {
            X_L[i][j] = X_L[i][X_L[i][j] - 1];
            if (X_L[i][j] == 0) {
              break;
            }
          }
        }
      }
      for (i = 0; i < m; ++ i) {
        for (j = n - 2; j >= 0; -- j) {
          while (H[i][X_R[i][j] + 1] >= H[i][j]) {
            X_R[i][j] = X_R[i][X_R[i][j] + 1];
            if (X_R[i][j] == 0) {
              break;
            }
          }
        }
      }

      int max = 0;
      for (i = 0; i < m; ++ i) {
        for (j =0; j < n; ++ j) {
          int cur = H[i][j] * (X_R[i][j] - X_L[i][j] + 1);
          if (H[i][j] && max < cur) {
            max = cur;
          }
        }
      }
      printf("%d\n", max * 3);
    }
  }
  return 0;
}

