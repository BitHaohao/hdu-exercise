/* Switch Game
 * 1. 求第n盏灯在n次switch之后的状态，只是求它的因子个数的奇偶性
 * 2. 再继续想一下，只有当一个数具有整数平方跟的时候，它的因子个数才是奇数个
 */
#if 0
#include <cstdio>
#include <cstdlib>

int main() {
  int cnt = 0, n = 0;
  while (EOF != scanf("%d", &n)) {
    for (int i = 1; i <= n; ++ i) {
      if (n % i == 0) {
        ++ cnt;
      }
    }
    printf("%d\n", cnt & 1);
    cnt = 0;
  }
  return 0;
}
#else
#include <cstdio>
#include <cstdlib>
#include <cmath>

inline bool func_isSquare(int n) {
  int x = sqrt(n);
  return n == x * x;
}

int main() {
  int n = 0;
  while (EOF != scanf("%d", &n)) {
    printf("%d\n", func_isSquare(n));
  }
  return 0;
}
#endif

