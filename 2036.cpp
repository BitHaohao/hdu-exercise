/* 改革春风吹满地
 * Assume (0,0) is always the first point.
 * S = Soab + Sobc + Socd + Soda 
 */
#include <cstdio>

int main() {
  int n = 0;
  double x1 = 0.f, y1 = 0.f;
  double x2 = 0.f, y2 = 0.f;
  double x3 = 0.f, y3 = 0.f;
  double area = 0.f;
  while(EOF != scanf("%d", &n) && n) {
    scanf("%lf%lf", &x1, &y1);
    x3 = x1;
    y3 = y1;
    while(-- n) {
      scanf("%lf%lf", &x2, &y2);
      area += (x1 * y2 - y1 * x2);
      x1 = x2;
      y1 = y2;
    }
    area += (x1 * y3 - y1 * x3);
    printf("%.1lf\n", area / 2);
    area = 0.f;
  }
  return 0;
}

