/* 人见人爱A-B
 */
#if 0
#include <cstdio>
#include <cstdlib>

#define MAX_LEN 101

int compare(const void *a, const void *b) {
  return *(int *)a - *(int *)b;
}

int main() {
  int arr[MAX_LEN] = {0};
  int m = 0, n = 0, elem = 0;
  while (EOF != scanf("%d %d", &m, &n) && (m | n)) {
    for (int i = 0; i < m; ++ i) {
      scanf("%d", arr + i);
    }
    for (int i = 0; i < n; ++ i) {
      scanf("%d", arr + m);
      int j = 0;
      for (; j < m; j ++) {
        if (arr[j] == arr[m]) {
          break;
        }
      }
      if (j != m) {
        arr[j] = arr[-- m];
      }
    }
    if (m == 0) {
      printf("NULL\n");
      continue;
    }
    qsort(arr, m, sizeof(int), compare);
    for (int k = 0; k < m; ++ k) {
      printf("%d ", arr[k]);
    }
    printf("\n");
  }
  return 0;
}
#else
#include <cstdio>
#include <set>

int main() {
  int m = 0, n = 0, elem = 0;
  std::set<int> arr;
  std::set<int>::iterator iter;
  while(EOF != scanf("%d %d", &m, &n) && (m | n)) {
    while(m --) {
      scanf("%d", &elem);
      arr.insert(elem);
    }
    for (int i = 0; i < n; ++ i) {
      scanf("%d", &elem);
      if (arr.count(elem)) {
        arr.erase(elem);
      }
    }
    if (arr.empty()) {
      printf("NULL\n");
    } else {
      for (iter = arr.begin(); iter != arr.end(); ++ iter) {
        printf("%d ", *iter);
      }
      printf("\n");
    }
    arr.clear();
  }
  return 0;
}
#endif

