/* Rectangles
 */
#include <cstdio>
#include <cstdlib>
#include <algorithm>

int main() {
  double x[4], y[4];
  while (EOF != scanf("%lf%lf", x, y)) {
    scanf("%lf%lf", x + 1, y + 1);
    scanf("%lf%lf", x + 2, y + 2);
    scanf("%lf%lf", x + 3, y + 3);
    if (x[0] > x[1]) std::swap(x[0], x[1]);
    if (y[0] > y[1]) std::swap(y[0], y[1]);
    if (x[2] > x[3]) std::swap(x[2], x[3]);
    if (y[2] > y[3]) std::swap(y[2], y[3]);
    
    if (x[0] < x[2]) std::swap(x[0], x[2]);
    if (x[1] > x[3]) std::swap(x[1], x[3]);
    if (y[0] < y[2]) std::swap(y[0], y[2]);
    if (y[1] > y[3]) std::swap(y[1], y[3]);
    printf("%.2f\n", x[0] < x[1] && y[0] < y[1] ?
      (y[1] - y[0]) * (x[1] - x[0]) : 0);
  }
  return 0;
}
