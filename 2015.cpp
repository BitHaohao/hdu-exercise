/* 偶数求和
 */
#include <cstdio>

static inline
int sumN(int n) {
  return (n * n + n);
}

int main() {
  int len = 0, step = 0;
  int suma = 0, sumb = 0, sum = 0;
  while (EOF != scanf("%d %d", &len, &step)) {
    int i = step;
    if (len > step) {
      sum = sumN(step);
      printf("%d", sum / step);
    } else {
      sum = sumN(len);
      printf("%d\n", sum / len);
      continue;
    }
    for (; i < len - step + 1; i += step) {
      sum = sumN(i + step) - sumN(i);
      printf(" %d", sum / step);
    }
    if (i == len) {
      printf("\n");
    } else {
      printf(" %d\n", (sumN(len) - sumN(i)) / (len - i));
    }
  }
  return 0;
}
