#include <cstdio>

static inline
void func_printbit(int n) {
  if (n) {
    func_printbit(n >> 1);
    putchar('0' + (n & 0x1));
  }
}

int main() {
  int cur = 0;
  while (EOF != scanf("%d", &cur)) {
    func_printbit(cur);
    putchar('\n');
  };
  return 0;
}

