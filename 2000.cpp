/* ASCII码排序
 */
#include <cstdio>
#include <algorithm>

int main() {
  char arr[4] = {0};
  while(EOF != scanf("%s", arr)) {
    if (arr[0] > arr[1]) { std::swap(arr[0], arr[1]); }
    if (arr[1] > arr[2]) { std::swap(arr[1], arr[2]); }
    if (arr[0] > arr[1]) { std::swap(arr[0], arr[1]); }
    printf("%c %c %c\n", arr[0], arr[1], arr[2]);
  }
  return 0;
}

