/* Treasure the new start, freshmen!
 */
#include <cstdio>

int main() {
  int N = 0, K = 0;
  float cre = 0.f, sco = 0.f;
  float credits = 0.f, scores = 0.f;
  float gpa = -1.f;
  bool has_gpa = true;
  scanf("%d", &N);
  while (N--) {
    scanf("%d", &K);
    while (K--) {
      scanf("%*s%f%f", &cre, &sco);
      if (!has_gpa) {
        continue;
      }
      if (sco < 60 && sco >= 0) {
        has_gpa = false;
      }
      credits += cre;
      scores += sco * cre;
    }
    if (has_gpa) {
      printf("%.2f\n", scores / credits);
    }
    else {
      printf("Sorry!\n");
    }
    credits = scores = 0.f;
    has_gpa = true;
    if (N) {
      printf("\n");
    }
  }
  return 0;
}
