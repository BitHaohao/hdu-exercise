/* Hano IV
 * f(n) = pow(3, n - 1) + 1
 */
#include <cstdio>

#define MAX_N 36
typedef unsigned long long uint64;

void func_hanoiv(uint64 *steps) {
  steps[0] = 0;
  uint64 tmp = 1;
  for (int i = 1; i < MAX_N; ++ i) {
    steps[i] = tmp + 1;
    tmp *= 3;
  }
}

int main() {
  uint64 pre_steps[MAX_N];
  func_hanoiv(pre_steps);
  int n = 0, m = 0;
  while(EOF != scanf("%d", &n)) {
    while (n --) {
      scanf("%d", &m);
      printf("%llu\n", pre_steps[m]);
    }
  }
  return 0;
}
