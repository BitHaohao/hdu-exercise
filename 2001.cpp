/* 计算两点之间的距离
 */
#include <cstdio>
#include <cmath>

int main() {
  float x1 = 0, y1 = 0, x2 = 0, y2 = 0;
  float x = 0, y = 0;
  while (EOF != scanf("%f %f %f %f", &x1, &y1, &x2, &y2)) {
    x = x1 - x2;
    y = y1 - y2;
    printf("%.2f\n", sqrt(x * x + y * y));
  }
  return 0;
}

