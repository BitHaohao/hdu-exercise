/* 1001 Sum Problem
 * If use for cycle to sum one-by-one, there's no risk that results
 *   exceeds 32bits inteager.
 * But when using this formula, square results may exceeds 32bits, so
 *   you shall use 64bits multiply instead of a 32bits.
 */
#include <cstdio>

int main() {
  int a = 0;
  while(EOF != scanf("%d", &a)) {
    int sum = ((long long)a * a + a) >> 1;
    printf("%d\n\n", sum);
  }
  return 0;
}
