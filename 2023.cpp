/* 求平均成绩
 * 成绩不一定就是小于等于100的正整数, 累加时候一定用64位整数
 */
#include <cstdio>
#include <cstring>

#define MAX_STU 50
#define MAX_COU 5

int main() {
  long long avg_cou[MAX_COU] = {0};
  long long avg_stu[MAX_STU] = {0};
  int stu_cou[MAX_STU][MAX_COU] = {0};
  int n = 0, m = 0, x = 0, sum = 0;
  while (EOF != scanf("%d%d", &n, &m)) {
    memset(avg_cou, 0, sizeof(avg_cou));
    memset(avg_stu, 0, sizeof(avg_stu));
    sum = 0;
    for (int i = 0; i < n; ++ i) {
      for (int j = 0; j < m; ++ j) {
        scanf("%d", &x);
        stu_cou[i][j] = x;
        avg_stu[i] += x;
        avg_cou[j] += x;
      }
    }
    for (int i = 0; i < n; ++ i) {
      printf("%.2lf%c", avg_stu[i] * 1.0 / m, i < n - 1 ? ' ' : '\n');
    }
    for (int i = 0; i < m; ++ i) {
      printf("%.2lf%c", avg_cou[i] * 1.0 / n, i < m - 1 ? ' ' : '\n');
    }
    for (int i = 0; i < n; ++ i) {
      bool flag = true;
      for (int j = 0; j < m; ++ j) {
        if (stu_cou[i][j] < avg_cou[j] * 1.0 / n) {
          flag = false;
          break;
        }
      }
      if (flag) {
        ++ sum;
      }
    }
    printf("%d\n\n", sum);
  }
}
