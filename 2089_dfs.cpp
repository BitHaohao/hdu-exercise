/* 不要62
 * 性能不如非递归版本, 但模式更通用
 */
#include <cstdio>
#include <cstring>

#define MAX_LEN (7+2)

int digit[MAX_LEN], len;
int dp[MAX_LEN][2];

// pos : 表示当前处理的是第pos位数
// stat : 标示是否已经出现了'6', 用于对当前位的'2'进行判断处理
// flag : 标识当前位以及[pos:1]有无上限限制
int dfs(int pos, bool stat, bool flag) {
  if (!pos) { // 初始状态, len=0, ret = 1
    return 1;
  }
  if (!flag && dp[pos][stat] != -1) { // 如果没有上区间限制, 且该数已经计算过
    return dp[pos][stat];             // 那么直接返回对应的数, 不需再dfs
  }
  int ret = 0;
  int end = flag ? digit[pos] : 9;
  for (int i = 0; i <= end; ++ i) {
    if (i == 4 || stat && i == 2) { // 当前位为'4' 或者 上一位为'6',当前位为'2'
      continue;
    }
    ret += dfs(pos - 1, i == 6, flag && i == end);
  }
  if (!flag) { // 没有上区间限制, 那么计算出来的ret就是完整的, 存下来用来重用
    dp[pos][stat] = ret;
  }
  return ret;
}

int getCount(int num) {
  len = 0;
  while (num > 0) {
    digit[++ len] = num % 10;
    num /= 10;
  }
  digit[len + 1] = 0;
  return dfs(len, false, true);
}

int main() {
  int frm = 1, to = 1;
  memset(dp, -1, sizeof(dp));
  while (EOF != scanf("%d%d", &frm, &to) && (frm | to)) {
    printf("%d\n", getCount(to) - getCount(frm - 1));
  }
  return 0;
}
