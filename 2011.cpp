/* 多项式求和
 */
#include <cstdio>

int main() {
  int n = 0, m = 0;
  double x = 0.f, y = 1.f;
  while (EOF != scanf("%d", &n)) {
    while (n --) {
      scanf("%d", &m);
      int pos = -1;
      for (int i = 2; i <= m; ++ i) {
        y += pos * (1 / float(i));
        pos = - pos;
      }
      printf("%.2lf\n", y);
      y = 1.f;
    }
  }
  return 0;
}

