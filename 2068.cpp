/* RPG的错排
 * 错排问题的变形
 * 注意，题目中说答对一半或以上，实际case则是答对一半以上
 * 以及数据的表示范围
 */
#include <cstdio>

#define MAX_N 26

void func_status(double *status, double *total) {
  status[0] = 1;
  status[1] = 0;
  status[2] = 1;
  total[0] = 1;
  total[1] = 1;
  total[2] = 2;
  for (int i = 3; i < MAX_N; ++ i) {
    total[i] = total[i - 1] * i;
    status[i] = (i - 1) * (status[i - 1] + status[i - 2]);
  }
}

int main() {
  double total_err[MAX_N];
  double factorial[MAX_N];
  func_status(total_err, factorial);
  int n = 0, m = 0;
  while(EOF != scanf("%d", &n) && n) {
    double x = 0;
    m = n / 2;
    for (int i = 0; i <= m; ++ i) {
      x += 1.0 * factorial[n] / factorial[n - i] / factorial[i] * total_err[i];
    }
    printf("%.0lf\n", x);
  }
  return 0;
}

