/* 汉字统计
 * 其实最关键的是输入中文的编码格式，Unicode / UTF-8 需严格区分
 */
#include <cstdio>

int main() {
  int n = 0, count = 0;
  char cur;
  while (EOF != scanf("%d%*c", &n) && n) {
    while (n --) {
      while ((cur = getchar()) != '\n') {
        if (cur < 0) {
          ++ count;
        }
      }
      printf("%d\n", count / 2);
      count = 0;
    }
  }
  return 0;
}
