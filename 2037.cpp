/* 今年暑假不AC
 * 简单贪心, 优先选择先结束的节目
 */
#include <cstdio>
#include <set>

typedef struct {
  int start;
  int end;
} Act;

// Sort by end time, then start time
bool operator < (const Act &a, const Act &b){
  long long ta = ((long long)a.end << 32) | a.start;
  long long tb = ((long long)b.end << 32) | b.start;
  return ta < tb;
}

int main() {
  int n = 0;
  std::set<Act> actlist;
  std::set<Act>::iterator iter;
  Act act;
  int cur = -1, count = 0;
  while(EOF != scanf("%d", &n) && n) {
    while (n --) {
      scanf("%d%d", &act.start, &act.end);
      actlist.insert(act);
    }
#if 0
    for (iter = actlist.begin(); iter != actlist.end(); ++ iter) {
      printf("%d %d\n", iter->start, iter->end);
    }
#endif
    for (iter = actlist.begin(); iter != actlist.end(); ++ iter) {
      if (cur <= iter->start) {
        cur = iter->end;
        ++ count;
      }
    }
    printf("%d\n", count);
    cur = -1;
    count = 0;
    actlist.clear();
  }
  return 0;
}


