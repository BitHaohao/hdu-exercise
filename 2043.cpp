/*
 */
#include <cstdio>
#include <ctype.h>

int main() {
  int type_a = 0, type_b = 0, type_c = 0, type_d = 0;
  int n = 0;
  char c = 0;
  while (EOF != scanf("%d\n", &n)) {
    while (n --) {
      scanf("%c", &c);
      while ('\n' != c) {
        if (isupper(c)) ++ type_a;
        else if (islower(c)) ++ type_b;
        else if (isdigit(c)) ++ type_c;
        else ++ type_d;
        scanf("%c", &c);
      }
      int len = type_a + type_b + type_c + type_d;
      if (len >16 || len < 8) {
        printf("NO\n");
      } else {
        int types = 0;
        if (type_a) ++ types;
        if (type_b) ++ types;
        if (type_c) ++ types;
        if (type_d) ++ types;
        if (types >= 3) {
          printf("YES\n");
        } else {
          printf("NO\n");
        }
      }
      type_a = type_b = type_c = type_d = 0;
    }
  }
  return 0;
}

