/* 素数判定
 */
#include <cstdio>
#include <algorithm>

int main() {
  int x = 0, y = 0;
  int tar = 0, idx = 0;
  bool is_prime = true;
  while (EOF != scanf("%d %d", &x, &y) && (x | y)) {
    if (x > y) {
      std::swap(x, y);
    }
    for (int i = x; i <= y; ++ i) {
      tar = i * i + i + 41;
      idx = sqrt(tar);
      for (int j = 2; j < idx; ++ j) {
        if (0 == tar % j) {
          is_prime = false;
          break;
        }
      }
      if (!is_prime) {
        break;
      }
    }
    if (is_prime) {
      printf("OK\n");
    } else {
      printf("Sorry\n");
    }
    is_prime = true;
  }
  return 0;
}