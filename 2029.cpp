#include <stdio.h>
#include <string.h>

#define MAX_LEN 1280

#if 0
char *strrev(char *str) {
  char *p1, *p2;
  if (! str || ! *str) {
    return str;
  }
  for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2) {
    *p1 ^= *p2;
    *p2 ^= *p1;
    *p1 ^= *p2;
  }
  return str;
}

int main() {
  static char str_ori[MAX_LEN] = {0};
  static char str_rev[MAX_LEN] = {0};
  int n;
  while (EOF != scanf("%d\n", &n) && n) {
    while (n --) {
      gets(str_ori);
      strcpy(str_rev, str_ori);
      strrev(str_rev);
      if (strcmp(str_ori, str_rev)) {
        puts("no");
      } else {
        puts("yes");
      }
    }
  }
  return 0;
}
#else
int main() {
  static char str[MAX_LEN] = {0};
  int n = 0, len = 0;
  while (EOF != scanf("%d\n", &n) && n) {
    while (n --) {
      gets(str);
      len = strlen(str);
      int i = 0, j = len - 1;
      for (; i < j; ++ i, -- j) {
        if (str[i] != str[j]) {
          puts("no");
          break;
        }
      }
      if (i >= j) {
        puts("yes");
      }
    }
  }
  return 0;
}
#endif