/* 最大连续子序列
 * 除状态转移公式的推导以外，重点在于对情况的合理分类
 *    第一个元素必须得单独保存下来
 */
#include <cstdio>

int main() {
  int n = 0, pre = 0, cur = 0, max = 0;
  int start = 0, end = 0, tmp = 0;
  int first = 0;
  while (EOF != scanf("%d", &n) && n) {
    scanf("%d", &first);
    tmp = start = end = pre = first;
    max = first;
    while(-- n) {
      scanf("%d", &cur);
      if (cur + pre < cur) {
        tmp = cur;
        pre = cur;
      } else {
        pre += cur;
      }
      if (max < pre) {
        max = pre;
        start = tmp;
        end = cur;
      }
    }
    if (max < 0) {
      printf("0 %d %d\n", first, cur);
    } else {
      printf("%d %d %d\n", max, start, end);
    }
  }
}
