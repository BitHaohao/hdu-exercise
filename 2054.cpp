/* A == B
 * 首先浮点数不一定就一定可以用double或float来表示
 * 其次数字长度可能非常长
 * 考虑正负，在本题目内可不考虑
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>

#define MAX_LEN (1024*16)

static inline
char *func_trimhead(char *arr) {
    while (arr[0] == '0') {
        ++ arr;
    }
    return arr;
}

static inline
void func_trimtail(char *arr) {
    int len = strlen(arr);
    char *tmp = arr + len - 1;
    if (strchr(arr, '.')) {
        while (*tmp == '0') {
            *tmp -- = 0;
        }
    }
    if (*tmp == '.') {
        *tmp = 0;
    }
}

int main() {
  char numa[MAX_LEN], numb[MAX_LEN];
  char *a, *b;
  while (EOF != scanf("%s%s", numa, numb)) {
    a = func_trimhead(numa);
    b = func_trimhead(numb);
    func_trimtail(a);
    func_trimtail(b);
    printf("%s\n", strcmp(a, b) ? "NO" : "YES");
  }
  return 0;
}
