/* 小兔的棋盘
 * 注意题目意思中是只考虑不通过(x,x)，终点为(n,n)的最短路径个数
 * 上下又是等价，所以只考虑上三角，只能向右或者乡下移动
 * 此时f(x,y)有
 *    f(0, 0) = 0;
 *    f(1:end, 0) = 1;
 *    f(i, i) = f(i, i-1)
 *    f(i, j) = f(i, j-1) + f(i-1,j)
 * 避免使用二维矩阵进行运算，会增加运算量及存储消耗，仔细推敲，一维矩阵就可以了
 */
#include <cstdio>
#include <cstdlib>

#define MAX_N 36
typedef unsigned long long uint64;

void func_steps(uint64 *arr) {
  arr[0] = 0;
  for (int i = 1; i < MAX_N; ++ i) {
    arr[i] = 1;
  }
  for (int i = 1; i < MAX_N; ++ i) {
    for (int j = i + 1; j < MAX_N; ++ j) {
      arr[j] += arr[j - 1];
    }
  }
}

int main() {
  int n = 0;
  uint64 m = 0;
  uint64 steps[MAX_N];
  func_steps(steps);
  while (EOF != scanf("%d", &n) && n != -1) {
    ++ m;
    printf("%llu %d %llu\n", m, n, steps[n] << 1);
  }
  return 0;
}

