/* Snooker
 */
#include <cstdio>

int main() {
  int bleft = 0, pscore = 0, oscore = 0;
  int n = 0;
  while (EOF != scanf("%d", &n) && n) {
    while (n--) {
      scanf("%d%d%d", &bleft, &pscore, &oscore);
      if (bleft > 6) {
        pscore += 8 * bleft - 21;
      }
      else {
        pscore += bleft * (15 - bleft) / 2;
      }
      printf("%s\n", pscore >= oscore ? "Yes" : "No");
    }
  }
  return 0;
}

