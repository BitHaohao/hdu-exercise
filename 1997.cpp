#include <cstdio>
#include <cstdlib>

#define MAX_N 65

bool hano_vii(int n, int *left, int *middle, int *right) {
  if (middle[0] == n) {
    return false;
  } else if (left[0] == n) {
    return hano_vii(n - 1, left + 1, right, middle);
  } else if (right[0] == n) {
    return hano_vii(n - 1, middle, left, right + 1);
  }
  return true;
}

int main() {
  int A[MAX_N], B[MAX_N], C[MAX_N];
  int t;
  int m, p, q, n;
  while(EOF != scanf("%d", &t)) {
    while(t --) {
      scanf("%d", &n);
      scanf("%d", &m);
      for (int i = 0; i < m; ++ i) {
        scanf("%d", A + i);
      }
      scanf("%d", &p);
      for (int i = 0; i < p; ++ i) {
        scanf("%d", B + i);
      }
      scanf("%d", &q);
      for (int i = 0; i < q; ++ i) {
        scanf("%d", C + i);
      }
      A[m] = B[p] = C[q] = -1;
      printf("%s\n", hano_vii(n, A, B, C) ? "true" : "false");
    }
  }
  return 0;
}

