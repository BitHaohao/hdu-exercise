/*
 */
#include <cstdio>
#include <cmath>

int main() {
  int n = 0, m = 0;
  double x = 0.f, sum = 0.f;
  while(EOF != scanf("%d %d", &m, &n)) {
    x = m;
    for (sum = 0; n --; x = sqrt(x)) {
      sum += x;
    }
    printf("%.2lf\n", sum);
  }
  return 0;
}

