/* C语言合法标识符
 * 执行输出不正确, 但是调试输出却正确, 没想明白
 */
#include <cstdio>
#include <cstring>
#include <ctype.h>

#define MAX_LEN 51

static inline
bool func_islegal(char *symbol) {
  if('_' != symbol[0] && !isalpha(symbol[0])) {
    return false;
  }
  for (int i = 1; symbol[i]; ++ i) {
    if (!isalnum(symbol[i]) && '_' != symbol[i]) {
      return false;
    }
  }
  return true;
}

int main() {
  int n = 0;
  char symbol[MAX_LEN] = {0};
  while (EOF != scanf("%d\n", &n)) {
    while (n --) {
      gets(symbol);
      if (func_islegal(symbol)) {
        puts("yes");
      } else {
        puts("no");
      }
    }
  }
}
