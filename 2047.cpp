/* 阿牛的EOF牛肉串
 * n    M(末尾含0)  N(末尾不含0)
 * 1    1          2
 * 2    N(1)       2 * (M(1) + N(1))
 * 3    N(2)       2 * (M(2) + N(2))
 * ...
 */
#include <cstdio>

#define MAX_N 41
typedef long long int64;

int main() {
  static int64 methods[MAX_N][3] = {{1, 2, 3}};
  for (int i = 1; i < MAX_N; ++ i) {
    methods[i][0] = methods[i - 1][1];
    methods[i][1] = 2 * (methods[i - 1][0] + methods[i - 1][1]);
    methods[i][2] = methods[i][0] + methods[i][1];
  }
  int n = 0;
  while (EOF != scanf("%d", &n)) {
    printf("%lld\n", methods[n - 1][2]);
  }
  return 0;
}
