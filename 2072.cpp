#include <cstdio>
#include <map>

int main() {
  std::map <std::string, int> section;
  std::map <std::string, int>::iterator it;
  std::string word = "";
  char   letter;
  while ((letter = getchar()) != '#') {
    it = section.begin();
    while (letter != '\n') {
      while (letter == ' ') {
        letter = getchar();
      }
      if (letter == '\n') break;

      while (letter != ' ' && letter != '\n') {
        word += letter;
        letter = getchar();
      }
      it = section.find(word);
      if (section.end() != it) {
        it->second += 1;
      } else {
        section.insert(it, std::pair<std::string, int>(word, 1));
      }
      word = "";
    }
    printf("%d\n", section.size());
    section.clear();
  }
  return 0;
}


