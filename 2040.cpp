/* 亲和数
 */
#include <cstdio>

static inline
int GetNum(int n) {
  int sum = 1, thresh = n >> 1;
  for(int i = 2; i <= thresh; ++ i) {
    if (n % i == 0) {
      sum += i;
    }
  }
  return sum;
}

int main() {
  int n  = 0, a = 0, b = 0;
  while (EOF != scanf("%d", &n)) {
    while (n --) {
      scanf("%d%d", &a, &b);
      printf("%s\n", GetNum(a) == b && a == GetNum(b) ? "YES" : "NO");
    }
  }
  return 0;
}

