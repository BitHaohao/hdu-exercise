#include <cstdio>
#include <cstring>

#define MAX_DSIZE 75

char str1 [MAX_DSIZE + 3];
char str2 [MAX_DSIZE + 3];

void func_print1(int n) {
  static int width = 0;
  if (n != width) {
    str1[0] = '+';
    memset(str1 + 1, '-', n);
    str1[n + 1] = '+';
    str1[n + 2] = '\0';
  }
  puts(str1);
  width = n;
}

void func_print2(int m) {
  static int width = 0;
  if (m != width) {
    str2[0] = '|';
    memset(str2 + 1, ' ', m);
    str2[m + 1] = '|';
    str2[m + 2] = '\0';
  }
  puts(str2);
  width = m;
}

int main() {
  int m = 0, n = 0;
  while(EOF != scanf("%d%d", &n, &m)) {
    func_print1(n);
    for (int i = 0; i < m; ++ i) {
      func_print2(n);
    }
    func_print1(n);
    putchar('\n');
  }
  return 0;
}


