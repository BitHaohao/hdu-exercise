/* 求绝对值
 */
#include <cstdio>

int main() {
  double a = 0;
  while(EOF != scanf("%lf", &a)) {
    if (a < 0) {
      printf ("%.2lf\n", -a);
    } else {
      printf ("%.2lf\n", a);
    }
  }
  return 0;
}

