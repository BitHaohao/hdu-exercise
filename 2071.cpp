#include <cstdio>
#include <algorithm>

int main() {
  int n = 0, m = 0;
  float height = 0.f, maximum = -1.0f;
  while(EOF != scanf("%d", &n)) {
    while (n --) {
      scanf("%d", &m);
      while (m --) {
        scanf("%f", &height);
        maximum = std::max(height, maximum);
      }
      printf("%.2f\n", maximum);
      maximum = -1.f;
    }
  }
  return 0;
}

