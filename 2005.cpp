/* 第几天
 */
#include <cstdio>

int main() {
  static const short pre_sum[2][12] = {
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334},
    {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335} // 闰年
  };
  int y = 0, m = 0, d = 0;
  int leap = 0;
  while (EOF != scanf("%d/%d/%d", &y, &m, &d)) {
    if (0 == y % 100) {
      leap = y % 400 == 0;
    } else {
      leap = (y & 3) == 0;
    }
    printf("%d\n", pre_sum[leap][m - 1] + d);
  }
  return 0;
}

