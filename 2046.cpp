/* 骨牌铺方格
 */
#include <cstdio>

#define MAX_N 51

typedef long long int64;

int main() {
  static int64 steps[MAX_N] = {1, 1};
  for (int i = 2; i < MAX_N; ++ i) {
    steps[i] = steps[i - 1] + steps[i - 2];
  }
  int n = 0;
  while(EOF != scanf("%d", &n) && n) {
    printf("%lld\n", steps[n]);
  }
  return 0;
}