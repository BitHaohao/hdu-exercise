/* A+B for Input-Output Practice (VII)
 */
#include <cstdio>

int main() {
  int a = 0, b = 0;
  while(EOF != scanf("%d %d", &a, &b)) {
    printf("%d\n\n", a + b);
  }
  return 0;
}
