#include <cstdio>

static const
char num[] = {"0123456789ABCDEF"};

static
void func_basecvt(int n, int base) {
  if (!n) {
    return;
  }
  func_basecvt(n / base, base);
  putchar(num[n % base]);
}

int main() {
  int n = 0, r = 0;
  while (EOF != scanf("%d%d", &n, &r)) {
    if (n < 0) {
      putchar('-');
      n = - n;
    }
    func_basecvt(n, r);
    putchar('\n');
  }
  return 0;
}
