/* The sum problem
 */
#include <cstdio>
#include <cmath>

int main() {
  int N = 0, M = 0;
  while (EOF != scanf("%d%d", &N, &M) && (M | N)) {
    int n = sqrt(M * 2.f);
    int temp = 0;
    while (n) {
      temp = M - (n - 1) * n / 2;
      if (0 == temp % n) {
        printf("[%d,%d]\n", temp / n, temp / n + n - 1);
      }
      -- n;
    }
    printf("\n");
  }
  return 0;
}
