/* 发工资咯：）
 * 题目可作为DP简单入门学习
 */
#include <cstdio>

#define RMB_TYPE 6

#if 0
// study on simple dp
#define RMB_MAX 1000000
int func_dp(int num) {
  static const int V[RMB_TYPE] = {1, 2, 5, 10, 50, 100};
  int MIN[RMB_MAX] = {0};
  for (int i = 1; i <= num; ++ i) {
    MIN[i] = MIN[i - 1] + 1;
    for (int j = 0; j < RMB_TYPE; ++ j) {
      if (V[j] <= i && MIN[i - V[j]] + 1 < MIN[i]) {
        MIN[i] = MIN[i - V[j]] + 1;
      }
    }
  }
  return MIN[num];
}
#endif

int func_mod(int num) {
  static const int V[RMB_TYPE] = {100, 50, 10, 5, 2, 1};
  int sum = 0;
  for (int i = 0; i < RMB_TYPE; ++ i) {
    if (num >= V[i]) {
      sum += num / V[i];
      num %= V[i];
    }
  }
  return sum;
}

int main() {
  int n = 0, m = 0;
  while(EOF != scanf ("%d", &n) && n) {
    int sum = 0;
    while (n --) {
      scanf("%d", &m);
      sum += func_mod(m);
    }
    printf("%d\n", sum);
  }
  return 0;
}
