/* 数据的交换输出
 */
#include <cstdio>
#include <algorithm>

#define MAX_LEN 101

int main() {
  int n = 0, m = 0, len = 0;
  int arr[MAX_LEN] = {0};
  int min = 0, idx = 0;
  while (EOF != scanf("%d", &n) && n) {
    len = n;
    -- n;
    scanf("%d", &min);
    arr[0] = min;
    for (int i = 1; i <= n; ++ i) {
      scanf("%d", &m);
      arr[i] = m;
      if (m < min) {
        min = m;
        idx = i;
      }
    }
    if (idx != 0) {
      std::swap(arr[0], arr[idx]);
    }
    for (int i = 0; i < len - 1; ++ i) {
      printf("%d ", arr[i]);
    }
    printf("%d\n", arr[len-1]);
    idx = 0;
  }
  return 0;
}

