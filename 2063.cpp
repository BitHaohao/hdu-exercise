#include <cstdio>
#include <stack>
#include <cstring>

#define MAX_NODES 512

typedef struct node {
  short pair;
  node *next;
}Node, *List;

short set_XX[MAX_NODES] = { 0 },
      set_XY[MAX_NODES] = { 0 };
short size_XX = 0;
short size_XY = 0;
Node  list[MAX_NODES];
char  visit[MAX_NODES];
std::stack<short> cross;

bool func_dfs(short x) {
  Node* p;
  cross.push(x);
  for (p = list[x].next; p; p = p->next) {
    if (!visit[p->pair]) {
      visit[p->pair] = true;
      cross.push(p->pair);
      if (!set_XY[p->pair] || func_dfs(set_XY[p->pair])) {
        return true;
      } else {
        cross.pop();
      }
    }
  }
  cross.pop();
  return false;
}

int main() {

  short K = 0;
  while (EOF != scanf("%hd", &K) && K) {
    scanf("%hd%hd", &size_XX, &size_XY);

    // init
    memset(set_XX, 0, MAX_NODES * sizeof(short));
    memset(set_XY, 0, MAX_NODES * sizeof(short));
    memset(list, 0, MAX_NODES * sizeof(Node));

    // input
    short x = 0, y = 0;
    Node *p = list;
    for (int i = 0; i < K; ++ i) {
      scanf("%hd%hd", &x, &y);
      p = new Node;
      p->pair = y;
      p->next = list[x].next;
      list[x].next = p;
    }

    // hungarian
    for (int i = 1; i <= size_XX; ++ i) {
      while(!cross.empty()) {
        cross.pop();
      }
      memset(visit, 0, (size_XY + 1) * sizeof(char));
      if (func_dfs(i)) {
        while (!cross.empty()) {
          short top = cross.top();
          cross.pop();
          set_XY[top] = cross.top();
          set_XX[cross.top()] = top;
          cross.pop();
        }
      }
    }

    // count pairs
    short count = 0;
    for (int i = 1; i <= size_XX; ++ i) {
      if (set_XX[i]) {
        ++ count;
      }
      while (p = list[i].next) {
        list[i].next = p->next;
        delete p;
      }
    }
    printf("%hd\n", count);

  }

  // clear stack if it's not empty
  while(!cross.empty()) {
    cross.pop();
  } 
  return 0;
}

