/* A+B for Input-Output Pratice(II)
 */
#include <cstdio>

int main() {
  int n = 0, a = 0, b = 0;
  scanf("%d", &n);
  while(n --) {
    scanf("%d %d", &a, &b);
    printf("%d\n", a + b);
  }
  return 0;
}

