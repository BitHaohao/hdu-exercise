#include <cstdio>
#include <cstdlib>
#include <climits>
#include <algorithm>
#include <cstring>

#define MAX_N 1002
typedef unsigned int uint32;
typedef signed   int int32;

int main() {
  uint32 path[MAX_N][MAX_N];
  char visited[MAX_N];
  int T = 0, S = 0, D = 0;
  uint32 a = 0, b = 0, dist = 0;
  uint32 mcityn = 0;
  while (EOF != scanf("%d%d%d", &T, &S, &D)) {
    memset(path, 0x3F, sizeof(path)); // cannot be 0xFF due to max have to add another value.
    memset(visited, 0, sizeof(visited));
    mcityn = 0;
    for (int i = 0; i < T; ++ i) {
      scanf("%u%u%u", &a, &b, &dist);
      path[a][b] = path[b][a] = std::min(dist, path[a][b]);
      mcityn = std::max(a, mcityn);
      mcityn = std::max(b, mcityn);
    }

    for (int i = 0; i < S; ++ i) {
      scanf("%u\n", &a);
      path[0][a] = 0;
    }

    // ShortestPath DIJ
    for (int i = 1; i <= mcityn; ++ i) {
      uint32 min_dist = UINT_MAX;
      int v = 0;
      for (int j = 1; j <= mcityn; ++ j) {
        if (!visited[j] && min_dist > path[0][j]) {
          min_dist = path[0][j];
          v = j;
        }
      }
      if (min_dist == UINT_MAX) {
        break;
      }
      visited[v] = 1;
      for (int j = 1; j <= mcityn; ++ j) {
        if (!visited[j] && (min_dist + path[v][j] < path[0][j])) {
          path[0][j] = min_dist + path[v][j];
        }
      }
    }

    uint32 min_dist = UINT_MAX;
    for (int i = 0; i < D; ++ i) {
      scanf("%u", &a);
      min_dist = std::min(min_dist, path[0][a]);
    }
    printf("%u\n", min_dist);
  }
  return 0;
}
