/* 字符串统计
 * Notice the '\n' after input n.
 * ctype.h has a function isdigit() to judge whether content in a char is 
 *    a digital number.
 */
#include <cstdio>
#include <ctype.h>

int main() {
  int n = 0, count = 0;
  char c = 0;
  while(EOF != scanf("%d\n", &n)) {
    while(n --) {
      scanf("%c", &c);
      while(c != '\n') {
        if (isdigit(c)) {
          ++ count;
        }
        scanf("%c", &c);
      }
      printf("%d\n", count);
      count = 0;
    }
  }
  return 0;
}

