/* 水仙花数
 * Note that there's just four narcissistic number between [100, 999], which are:
 *    153, 370, 371, 407
 */
#include <cstdio>
#include <algorithm>

#if 0
int main() {
  static const int cube[11] = {
    0, 1, 8, 27, 64, 125, 216, 343, 512, 729
  };
  int count = 0;
  int frm  = 0, to = 0;
  while (EOF != scanf("%d %d", &frm, &to)) {
    if (frm > to) {
      std::swap(frm, to);
    }
    for (int i = frm; i <= to; ++ i) {
      int x = i / 100;
      int y = (i % 100) / 10;
      int z = i % 10;
      int temp = cube[x] + cube[y] + cube[z];
      if (i == temp) {
        if (count != 0) {
          printf(" %d", i);
        }
        else {
          printf("%d", i);
        }
        count ++;
      }
    }
    if (count == 0) {
      printf("no\n");
    } else {
      printf("\n");
    }
    count = 0;
  }
  return 0;
}
#else
int main() {
  int frm  = 0, to = 0;
  int count = 0;
  while (EOF != scanf("%d %d", &frm, &to)) {
    if (frm > to) {
      std::swap(frm, to);
    }
    if (frm <= 153 && to >= 153) {
      printf("153");
      ++ count;
    }
    if (frm <= 370 && to >= 370) {
      if (count) {
        printf(" 370");
      } else {
        printf("370");
      }
      ++ count;
    }
    if (frm <= 371 && to >= 371) {
      if (count) {
        printf(" 371");
      } else {
        printf("371");
      }
      ++ count;
    }
    if (frm <= 407 && to >= 407) {
      if (count) {
        printf(" 407");
      } else {
        printf("407");
      }
      ++ count;
    }
    if (count) {
      printf("\n");
    } else {
      printf("no\n");
    }
    count = 0;
  }
  return 0;
}
#endif

