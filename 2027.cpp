#include <cstdio>
#include <cstring>
#include <ctype.h>

#define MAX_LEN 100

int main() {
  static char entity[MAX_LEN + 1] = {0};
  char aeiou[5] = {0};
  int n = 0;
  while(EOF != scanf("%d\n", &n) && n) {
    while(n --) {
      memset(aeiou, 0, sizeof(aeiou));
      gets(entity);
      for (int i = 0; entity[i]; ++ i) {
        switch(entity[i]) {
          case 'a': ++ aeiou[0]; break;
          case 'e': ++ aeiou[1]; break;
          case 'i': ++ aeiou[2]; break;
          case 'o': ++ aeiou[3]; break;
          case 'u': ++ aeiou[4]; break;
        }
      }
      printf("a:%d\ne:%d\ni:%d\no:%d\nu:%d\n",
             aeiou[0], aeiou[1], aeiou[2], aeiou[3], aeiou[4]);
      if (n) {
        printf("\n");
      }
    }
  }
}
