/* An easy problem
 */
#include <cstdio>
#include <cstdlib>

int main() {
  int n = 0, y = 0;
  char x;
  scanf("%d%*c", &n);
  while (n--) {
    scanf("%c%d%*c", &x, &y);
    printf("%d\n", y + (x < 97 ? x - 'A' + 1 : 'a' - x - 1));
  }
  return 0;
}
