/* 成绩转换
 */
#include <cstdio>

int main() {
  int score = 0;
  while (EOF != scanf("%d", &score)) {
    if (score < 0 || score > 100) {
      printf("Score is error!\n");
      continue;
    }
    switch(score / 10) {
      case 10:
      case 9:
        printf("A\n");
        continue;
      case 8:
        printf("B\n");
        continue;
      case 7:
        printf("C\n");
        continue;
      case 6:
        printf("D\n");
        continue;
      default:
        printf("E\n");
        continue;
    }
  }
  return 0;
}

