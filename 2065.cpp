/* "红色病毒问题"
 * 题意中是求ABCD的排列数，A和C要么不出现，要么必须为偶数个才算符合条件
 * 根据其指数型生成函数，可推导得方案总数为：
 * f(n) = pow(4, n-1) + pow(2, n-1)
 *      = pow(pow(2, n-1), 2) + pow(2, n-1)
 * 将f(n)对100取余
 * 求幂可用学习fibonacci数列时候的Exponentiation by Squaring方式快速求得
 * 以避免循环大数据
 */
#include <cstdio>
#include <cstdlib>

typedef unsigned long long uint64;

// Exponentiation by Squaring
int func_pow(int base, uint64 power, int mod) {
  int tmp = 1;
  while (power) {
    if (power & (uint64)1) {
      tmp = (tmp * base) % mod;
    }
    power >>= 1;
    base = (base * base) % mod;
  }
  return tmp;
}

int main() {
  int n = 0;
  uint64 x = 0;
  while(EOF != scanf("%d", &n) && n) {
    for (int i = 0; i < n; ++ i) {
      scanf("%llu", &x);
      int a = func_pow(2, x - 1, 100);
      int b = (a * a) % 100;
      printf("Case %d: %d\n", i + 1, (a + b) % 100);
    }
    printf("\n");
  }
  return 0;
}
