/* 数列有序!
 * 注意输出格式
 */
#include <cstdio>

#define MAX_LEN 101

int main() {
  static int arr[MAX_LEN];
  int m = 0, n = 0, i = 0;
  while (EOF != scanf("%d%d", &n, &m) && (n | m)) {
    for (i = 0 ; i < n ; i++) {
      scanf("%d", arr + i);
    }
    for (i = n ; i && arr[i - 1] > m ; i--) {
      arr[i] = arr[i - 1];
    }
    arr[i] = m;
    for (i = 0 ; i < n ; i++) {
      printf("%d ", arr[i]);
    }
    printf("%d\n", arr[i]);
  }
  return 0;
}