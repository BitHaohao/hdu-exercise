/* A+B for Input-Output Practice (III)
 */
#include <cstdio>

int main() {
  int a = 0, b = 0;
  while(EOF != scanf("%d %d", &a, &b) && (a | b)) {
    printf("%d\n", a + b);
  }
  return 0;
}

