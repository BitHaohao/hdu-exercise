/* A + B
 */
#include <cstdio>
#include <cstring>

static inline
int getNum(const char *num) {
  int mul = num[0] + 1;
  mul *= num[1];
  switch(mul) {
    case 12423:return 0;
    case 12320:return 1;
    case 13923:return 2;
    case 12168:return 3;
    case 11433:return 4;
    case 10815:return 5;
    case 12180:return 6;
    case 11716:return 7;
    case 10710:return 8;
    case 11655:return 9;
  }
  return -1;
}

int main() {
  char numa[20] = {0};
  char numb[20] = {0};
  int a = 0, b = 0, sum = 0;
  while (EOF != scanf("%s", numa)) {
    a = getNum(numa);
    scanf("%s", numa);
    if (strcmp(numa, "+")) {
      a *= 10;
      a += getNum(numa);
      scanf("%s", numa);
    }
    scanf("%s", numb);
    b = getNum(numb);
    scanf("%s", numb);
    if (strcmp(numb, "=")) {
      b *= 10;
      b += getNum(numb);
      scanf("%s", numb);
    }
    sum = a + b;
    if (sum) {
      printf("%d\n", a + b);
    }
  }
  return 0;
}

