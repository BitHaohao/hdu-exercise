/*  绝对值排序
 */
#include <cstdio>
#include <cstdlib>

#define MAX_LEN 101

int compare(const void *numa, const void *numb) {
  return std::abs(*(const int *)numb) - std::abs(*(const int *)numa);
}

int main() {
  static int arr[MAX_LEN];
  int x = 0, n = 0, i = 0;
  while (EOF != scanf("%d", &n) && n) {
    for (i = 0 ; i < n ; i++) {
      scanf("%d", arr + i);
    }

    std::qsort(arr, n, sizeof(int), compare);

    for (i = 0 ; i < n - 1 ; i++) {
      printf("%d ", arr[i]);
    }
    printf("%d\n", arr[i]);
  }
  return 0;
}