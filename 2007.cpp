/* 平方和与立方和
 * Mainly optimize by formula derivation, but data range and the order of input
 *    two number shall be highlight.
 */
#include <cstdio>

int main() {
  int m = 0, n = 0, temp = 0;
  long long odd = 0, even = 0;
  while (EOF != scanf("%d %d", &m, &n)) {
    if (m > n) {
      temp = m;
      m = n;
      n = temp;
    }
    int i = (m & 0x1) == 0 ? m : m + 1;
    int x = (m & 0x1) == 1 ? m : m + 1;
    if (i <= n) {
      long long j = (n - i) >> 1;
      even = i * i * (j + 1) + 2 * i * j * (j + 1) + 2 * j * (j + 1) * (2 * j + 1) / 3;
    }
    if (x <= n) {
      long long y = (n - x) >> 1;
      odd = (y + 1) * x * x * x + 3 * x * x * y * (y + 1) + 2 * x * y * (y + 1) * (2 * y + 1)
            + 2 * y * y * (y + 1) * (y + 1);
    }
    printf("%d %d\n", (int)even, (int)odd);
    even = odd = 0;
  }
  return 0;
}

