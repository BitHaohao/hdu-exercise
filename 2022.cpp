/* 海选女主角
 * int 不可用, long long 全搞定
 */
#include <cstdio>
#include <cmath>

int main() {
  int m = 0, n = 0;
  long long absmax = 0, max = 0, x = 0;
  int posx = 1, posy = 1;
  while (EOF != scanf("%d%d", &m, &n)) {
    for (int i = 1; i <= m; ++ i) {
      for (int j = 1; j <=n; ++ j) {
        scanf("%lld", &x);
        if (std::abs(x) > absmax) {
          max = x;
          absmax = std::abs(x);
          posx = i;
          posy = j;
        }
      }
    }
    printf("%d %d %d\n", posx, posy, max);
    absmax = max = 0;
    posx = posy = 1;
  }
  return 0;
}
